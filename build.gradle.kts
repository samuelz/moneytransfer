import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
	application
	kotlin("jvm") version "1.3.61"
	id("com.github.johnrengelman.shadow") version ("5.2.0")
}

repositories {
	mavenCentral()
	maven { url = uri("https://dl.bintray.com/konform-kt/konform") }
	jcenter()
}

dependencies {
	implementation(kotlin("stdlib"))
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")

	implementation("org.http4k:http4k-core:3.200.0")
	implementation("org.http4k:http4k-server-netty:3.200.0")
	implementation("org.http4k:http4k-client-okhttp:3.200.0")
	implementation("org.http4k:http4k-format-jackson:3.200.0")

	implementation("io.konform:konform:0.1.0")

	implementation("com.opentable.components:otj-pg-embedded:0.13.3")
	implementation("com.zaxxer:HikariCP:3.4.1")
	implementation("org.jetbrains.exposed:exposed:0.17.7")


	implementation("org.slf4j:slf4j-api:1.7.5")
	implementation("org.apache.logging.log4j:log4j-api:2.11.1")
	implementation("org.apache.logging.log4j:log4j-core:2.11.1")
	implementation("org.apache.logging.log4j:log4j-slf4j-impl:2.11.1")

	testImplementation("io.kotlintest:kotlintest-runner-junit5:3.4.2")

}

tasks.withType<Test> {
	useJUnitPlatform()
	failFast = false
	testLogging {
		showStandardStreams = true
		exceptionFormat = TestExceptionFormat.FULL
		events = setOf(TestLogEvent.STARTED, TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.STANDARD_OUT, TestLogEvent.STANDARD_ERROR)
		showExceptions = true
		showCauses = true
		showStackTraces = true
	}

	afterSuite(KotlinClosure2<TestDescriptor, TestResult, Unit>({ desc, result ->
		if (desc.parent == null) {
			var output = "Results: ${result.resultType} (${result.testCount} tests, ${result.successfulTestCount} successes, ${result.failedTestCount} failures, ${result.skippedTestCount} skipped)"
			val startItem = "|  "
			val endItem = "  |"
			val repeatLength = startItem.length + output.length + endItem.length
			println("\n" + ("-".repeat(repeatLength)) + "\n" + startItem + output + endItem + "\n" + ("-".repeat(repeatLength)))
		}
	}))
}

tasks.test {
	exclude("samz/moneytransfer/stress/**")
}

task<Test>("loadTest") {
	description = "Runs a load/stress test"
	exclude("samz/moneytransfer/api/**")
}


application {
	mainClassName = "samz.moneytransfer.LauncherKt"
}


val fatJar by tasks.creating(Jar::class) {
	manifest {
		attributes["Main-Class"] = application.mainClassName
	}
	archiveBaseName.set("${project.name}-fat")
	from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
	with(tasks["jar"] as CopySpec)
}

artifacts {
	archives(fatJar)
}



