# Money transfer example

## DISCLAIMER

I'm far away from being a kotlin expert, so don't expect to see idiomatic kotlin.  
I've been doing mostly Scala in last years, and my Kotlin experience is from me pushing to use it on some small projects.    

There are some areas, especially controllers, where I neglected DRY in favor of some copy paste.  
In the same areas I also could've refactored/abstracted for more DRY & Readable code.

Instead of proper Dependency Injection, I do some ugly initialization and also "abuse" Kotlins lazy object initialization.  

I decided to do it this way so I could concentrate on the more interesting part of the problem,
namely the "money transfer execution" part of it.

Most of the other solutions on the web (public github/bitbucket, what did you expect?) use some sort of 
explicit "syncronization" or locking method.
This is "ok-ish" on small scale app or an app that is fully run within the same process (or jvm).

But for a massive-scale distributed app, you just need more!

I wanted to present you with another solution where the REST API only acts as a the first receiver of a "transfer request"
from the end user, then passing it on to a non-web consumer for processing.
The consumer uses db level row locks (select for update) + transaction isolation to achieve correct balance updates.    

I consider this to be a "mini" or Proof-of-Concept version of your architecture: 

![architecture.png](architecture.png)


## Libs & versions used

- runtime jvm: openjdk 11.0.2
- programming language: Kotlin 1.3.61
- build tool: Gradle 6.0.1 
- web server: http4k + netty
- db: in-memory postgresql + hikariCP connection pool
- light orm: exposed
- test framework: kotlintest
- validation: konform
- logging: SLF4J (abstraction) + log4j (impl)


#### lib considerations:


http4k is nice. its production ready and mature enough.  
instead of being a full-blown framework it is a bunch of libs you can choose from.  

exposed orm: is IMO a very simple ORM.  
I would generally rather use a 'lower-level' sql generation lib.  
But for sake of saving time I used this anyway.

db: I wanted a real db and not some H2 fake db.  
testing should be done on a system as close to the real one.  
since you requested "no containers" & "single jar" I looked for and found this in-memory postgresql lib.  

validation: konform is small & cute, i'd prefer something more mature for real live app.

                                    
## Challenge statement

REST API for money transfers between accounts that can withstand high load of requests.


## Build & Run 

From the project root run:

```bash
./gradlew clean shadowJar
java -jar build/libs/money-transfer-example-all.jar
```

after running, `ctrl + c` will initiate a graceful shutdown.  


## Interacting with api

web server is started on port 9000.

2 accounts with some balance are created on server startup for convenience, ids 1 & 2.
 

ping:  

```bash
curl localhost:9000/ping
```  

create account:
  
```bash
curl -X POST -v localhost:9000/account -d '{"nickname": "some nickname" }'
```

get account:  

```bash
curl localhost:9000/account/{id}
```

get transfer by id:   

```bash
curl localhost:9000/money/transfer/{id}
```

the following charge & transfer endpoints return just the id of the pending transfer.

charge (also called top-up sometimes):  

```bash
curl -X POST -v localhost:9000/money/charge -d '{"amount": 1, "accountId":1 }'
```

transfer money:  

```bash
curl -X POST -v localhost:9000/money/transfer -d '{"amount": 1, "accountId":1, "otherAccountId": 2 }'
```


## Tests

From the project root run:

```bash
./gradlew test
```

for load/stress tests:

```bash
./gradlew loadTest
```


## Some things that are important to me in a code submission

#### e2e testing: 

with the limited time I have, I prefer to focus on testing everything as a whole and not just units.   


#### not throwing errors for flow control

Kotlin (and Scala) offer so much in term of flow control.  
why throw errors when the stacktrace is not important or not used?  
instead one can use sealed classes as return values or an Either/Result


#### modules & separation of concerns

ideally the project would be separated into modules: api, db, processor, shared (models).  
I did not want to spend too much time on build configuration.  
I made the code "module-ready" as much as I could.


## About the solution: implementation decisions

#### amounts: no decimals

all amounts are `Int`s and represent the amount in cents (or whatever small unit is called for a currency).
presentation layer (not part of project scope) can take care of formatting according to currency standards. 


#### balance

the account does NOT have balance! what?!    

I chose to treat "transfers" as some sort of "log" .  
At account creation, an "init" transfer is created, the start of the log for the account.  
reading through the history of transfers for an account will show you all the actions that affected balance.  
each transfer shows the balance after it was made.  
so the latest transfer of an account carries the current balance.


#### message queue

I chose a Deque Blocking Queue as the message queue, so I could put the END message at the beginning.    
in production this could be rabbitmq, kafka, aws sqs etc


#### transfer processer & non-locks

non-locks is a lie :)
you must force order somehow when dealing with this kind of situation.
i used the db level with a `sql for update` kind of statement to force a lock on the account that is currently having a transfer executed.
there is also a safety check against a state (lastTransfer) that must be maintained in order to complete a transfer execution and update balance.  
if the state has changed (checked within subquery), the state is refreshed and execution is retried.     


#### sync code everywhere

In order to avoid extra complexity, I avoided using Futures or coroutines.  
The web server (api) already has multiple workers to handle requests, so its not that bad in performance. 


## Possible Problems:

#### user spams with many transfers? 

can add rate limiting on endpoint, considering user/account id as key

#### account blocked after initiating transfer (and its still pending)?

IMO this is a calculated & known risk in finance.    
It can be reduced by not allowing account to "cash out" after being blocked:
    - attempt to ATM cash out? block
    - made outgoing SEPA? send out a cancellation

OR by introducing "delays" that give agents time to cancel account transfers.      
for example N26 delays usage of incoming p2p money (cant use it till a day after).

#### multiple "transfer workers" take multiple transfers of user?

the workers with "newer" transfers (id is higher) give way to the "oldest" pending one by returning their message back to queue.

#### multiple transfers lead to balance probs on a specific account?

In real life there are'nt many transfers happening at once between 2 specific accounts.  
so IMO the risk probability is not high.  

can de-risk this by introducing "throttling" of how many transfers per minute allowed on an account.  
or even only allowing 1 transfer in progress for accounts per transfer type.


## Final thoughts

this was a fun challenge.  
got me to re-read and refresh some locking concepts and practice my kotlin some more :)

sorry for the long README.   
I like to spell out my thoughts, concerns & considerations that can not otherwise be conveyed with code alone.
