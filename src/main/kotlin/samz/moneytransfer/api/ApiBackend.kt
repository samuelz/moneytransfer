package samz.moneytransfer.api

import org.http4k.core.HttpHandler
import org.http4k.core.RequestContexts
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.server.Http4kServer
import org.http4k.server.Netty
import org.http4k.server.asServer
import samz.moneytransfer.api.controllers.AccountController
import samz.moneytransfer.api.controllers.HealthCheckController
import samz.moneytransfer.api.controllers.MoneyTransferController
import samz.moneytransfer.api.services.AccountService
import samz.moneytransfer.api.services.MoneyTransferService
import samz.moneytransfer.db.AccountsRepo
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.processor.ProcessorMessage
import java.util.concurrent.BlockingQueue

class ApiBackend(private val port: Int,
								 transferProcessorQueue: BlockingQueue<ProcessorMessage>) : Http4kServer {

	private val contexts = RequestContexts()

	// poor mans alternative to dependency injection
	private val accountsRepo = AccountsRepo()
	private val transfersRepo = TransfersRepo()
	private val accountService = AccountService(accountsRepo, transfersRepo)
	private val moneyTransferService = MoneyTransferService(transfersRepo, transferProcessorQueue, accountsRepo)
	private val accountController: AccountController = AccountController(accountService)
	private val healthCheckController: HealthCheckController = HealthCheckController()
	private val moneyTransferController: MoneyTransferController = MoneyTransferController(moneyTransferService, contexts)

	private val apiRoutes = Routes(healthCheckController, accountController, moneyTransferController).routes

	private val server =
			ServerFilters.InitialiseRequestContext(contexts)
					.then(routes())
					.asServer(Netty(port))


	private fun routes(): HttpHandler =
			ServerFilters.CatchAll()
					.then(ServerFilters.CatchLensFailure)
					.then(apiRoutes)

	override fun port(): Int {
		return port
	}

	override fun start(): Http4kServer {
		return server.start()
	}

	override fun stop(): Http4kServer {
		return server.stop()
	}
}

