package samz.moneytransfer.api.services

import samz.moneytransfer.api.controllers.MoneyTransferResponse
import samz.moneytransfer.api.controllers.MoneyTransferResponse.*
import samz.moneytransfer.api.controllers.Requests
import samz.moneytransfer.db.AccountsRepo
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.models.Transfer
import samz.moneytransfer.models.TransferType
import samz.moneytransfer.processor.ProcessorMessage
import java.util.concurrent.BlockingQueue


class MoneyTransferService(private val transfersRepo: TransfersRepo,
													 private val transferProcessorQueue: BlockingQueue<ProcessorMessage>,
													 private val accountsRepo: AccountsRepo) {

	fun charge(chargeRequest: Requests.ChargeTransferRequest): MoneyTransferResponse {
		val account = accountsRepo.getAccountById(chargeRequest.accountId)
				?: return AccountForTransferNotFound(chargeRequest.accountId)

		return transfersRepo.createPendingTransfer(
				chargeRequest.amount,
				account.id,
				account.id,
				TransferType.CHARGE
		).let { transferId ->
			transferProcessorQueue.put(ProcessorMessage.TransferMessage(transferId, account.id))
			MoneyTransferPending(transferId)
		}
	}

	fun transfer(moneyTransferRequest: Requests.MoneyTransferRequest): MoneyTransferResponse {
		val account = accountsRepo.getAccountById(moneyTransferRequest.accountId)
				?: return AccountForTransferNotFound(moneyTransferRequest.accountId)
		val otherAccount = accountsRepo.getAccountById(moneyTransferRequest.otherAccountId)
				?: return AccountForTransferNotFound(moneyTransferRequest.otherAccountId)

		if (moneyTransferRequest.accountId == moneyTransferRequest.otherAccountId)
			return SelfTransferNotAllowed

		return transfersRepo.createPendingTransfer(
				moneyTransferRequest.amount,
				account.id,
				otherAccount.id,
				TransferType.TRANSFER_DEBIT
		).let { transferId ->
			transferProcessorQueue.put(ProcessorMessage.TransferMessage(transferId, account.id))
			MoneyTransferPending(transferId)
		}
	}

	fun getTransferById(id: Int): Transfer? = transfersRepo.getTransferById(id)
}
