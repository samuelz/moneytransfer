package samz.moneytransfer.api.services

import samz.moneytransfer.db.AccountsRepo
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.models.Account

class AccountService(private val accountsRepo: AccountsRepo,
										 private val transfersRepo: TransfersRepo) {

	fun createAccount(nickname: String, startingBalance: Int = 0): Account {
		val accountId = accountsRepo.createAccount(nickname)
		transfersRepo.createInitAccountTransfer(accountId, startingBalance).also {
			accountsRepo.updateLastTransferId(accountId, it)
		}

		return accountsRepo.getAccountById(accountId)!!
	}

	fun getAccountById(id: Int): Account? = accountsRepo.getAccountById(id)

}
