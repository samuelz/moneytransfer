package samz.moneytransfer.api

import org.http4k.core.Method
import org.http4k.routing.bind
import samz.moneytransfer.api.controllers.AccountController
import samz.moneytransfer.api.controllers.HealthCheckController
import samz.moneytransfer.api.controllers.MoneyTransferController

class Routes(
		healthCheckController: HealthCheckController,
		accountController: AccountController,
		moneyTransferController: MoneyTransferController) {

	// my personal preference is to have a centralized place for route definitions
	// some prefer doing it at the controller level, or in the server definition
	val routes = org.http4k.routing.routes(
			"/ping" bind Method.GET to healthCheckController.ping,
			"/account" bind Method.POST to accountController.createAccount,
			"/account/{id}" bind Method.GET to accountController.getAccountById,
			"/money/charge" bind Method.POST to moneyTransferController.charge,
			"/money/transfer" bind Method.POST to moneyTransferController.transfer,
			"/money/transfer/{id}" bind Method.GET to moneyTransferController.getTransferById
	)
}
