package samz.moneytransfer.api.controllers

import io.konform.validation.Invalid
import io.konform.validation.Valid
import org.http4k.core.*
import org.http4k.format.Jackson.json
import samz.moneytransfer.api.controllers.JsonFormatters.asJson
import samz.moneytransfer.api.controllers.Lenses.CreateAccountRequestLens
import samz.moneytransfer.api.controllers.Lenses.pathIdIntLens
import samz.moneytransfer.api.controllers.Validators.createAccountRequestValidator
import samz.moneytransfer.api.services.AccountService


class AccountController(accountService: AccountService) {

	val createAccount: HttpHandler = { request ->
		when (val validationResult = createAccountRequestValidator(CreateAccountRequestLens(request))) {
			is Valid -> {
				accountService.createAccount(validationResult.value.nickname).let {
					Response(Status.OK)
							.with(Body.json().toLens() of asJson(it))
				}
			}
			is Invalid -> {
				Response(Status.BAD_REQUEST)
						.with(Body.json().toLens() of asJson(
								validationResult,
								listOf(Requests.CreateAccountRequest::nickname)))
			}
		}
	}

	val getAccountById: HttpHandler = { request ->
		accountService.getAccountById(pathIdIntLens(request))
				?.let {
					Response(Status.OK)
							.with(Body.json().toLens() of asJson(it))
				} ?: Response(Status.NOT_FOUND)
	}
}
