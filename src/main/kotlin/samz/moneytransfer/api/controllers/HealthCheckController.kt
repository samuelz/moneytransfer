package samz.moneytransfer.api.controllers

import org.http4k.core.HttpHandler
import org.http4k.core.Response
import org.http4k.core.Status

class HealthCheckController {

	val ping: HttpHandler = {
		Response(Status.OK).body("pong")
	}

	// this is where you could have more handlers such as:
	// - status of server
	// - endpoint to return version or config
	// - expose metrics (for Prometheus for example)
}
