package samz.moneytransfer.api.controllers

import com.fasterxml.jackson.databind.JsonNode
import io.konform.validation.Invalid
import org.http4k.core.Body
import org.http4k.format.Jackson.auto
import org.http4k.lens.Path
import org.http4k.lens.int
import samz.moneytransfer.models.Account
import samz.moneytransfer.models.Transfer
import kotlin.reflect.KProperty1
import org.http4k.format.Jackson as json

// this file is a mixed bag of objects that are reused in controllers
// in a real (bigger) project they would each get their own file and be
// better placed in the package hierarchy.

object Lenses {
	val pathIdIntLens = Path.int().of("id")

	val CreateAccountRequestLens = Body.auto<Requests.CreateAccountRequest>().toLens()
	val ChargeTransferRequestLens = Body.auto<Requests.ChargeTransferRequest>().toLens()
	val MoneyTransferRequestLens = Body.auto<Requests.MoneyTransferRequest>().toLens()

}

object Requests {
	data class CreateAccountRequest(val nickname: String)

	data class ChargeTransferRequest(val amount: Int, val accountId: Int)

	data class MoneyTransferRequest(
			val accountId: Int,
			val otherAccountId: Int,
			val amount: Int
	)
}

sealed class MoneyTransferResponse {
	data class MoneyTransferPending(val transferId: Int) : MoneyTransferResponse()
	object SelfTransferNotAllowed : MoneyTransferResponse()
	data class AccountForTransferNotFound(val accountId: Int) : MoneyTransferResponse()
}

object JsonFormatters {

	fun asJson(validationRes: Invalid<*>, propertiesToExtract: List<KProperty1<*, *>>): JsonNode {
		val errorsList = propertiesToExtract.map { it.name to (validationRes[it] ?: emptyList()) }
		return json {
			obj("errors" to obj(
					errorsList.map { it.first to array(it.second.map { msg -> string(msg) }) }))
		}
	}

	fun asJson(account: Account): JsonNode {
		return account.run {
			json {
				obj(
						"id" to number(id),
						"nickname" to string(nickname),
						"createdAt" to string(createdAt.toString("YYYY-MM-dd HH:mm:ss")),
						"updatedAt" to string(updatedAt.toString("YYYY-MM-dd HH:mm:ss"))
				)
			}
		}
	}

	fun asJson(pendingTransfer: MoneyTransferResponse.MoneyTransferPending): JsonNode {
		return pendingTransfer.run {
			json {
				obj(
						"id" to number(transferId))
			}
		}
	}

	fun asJson(transfer: Transfer): JsonNode {
		return transfer.run {
			json {
				obj(
						"id" to number(id),
						"createdAt" to string(createdAt.toString("YYYY-MM-dd HH:mm:ss")),
						"updatedAt" to string(updatedAt.toString("YYYY-MM-dd HH:mm:ss")),
						"finishedAt" to (finishedAt?.let { string(it.toString("YYYY-MM-dd HH:mm:ss")) } ?: nullNode()),
						"amount" to number(amount),
						"balanceAfter" to number(balanceAfter),
						"accountId" to number(accountId),
						"otherAccountId" to number(otherAccountId),
						"type" to string(`type`.name),
						"state" to string(state.name)
				)
			}
		}
	}
}
