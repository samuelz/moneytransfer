package samz.moneytransfer.api.controllers

import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.maximum
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.minimum

object Validators {

	val createAccountRequestValidator = Validation<Requests.CreateAccountRequest> {
		Requests.CreateAccountRequest::nickname required {
			minLength(1)
			maxLength(50)
		}
	}

	val chargeTransferRequestValidator = Validation<Requests.ChargeTransferRequest> {
		Requests.ChargeTransferRequest::amount required {
			minimum(1)
			maximum(1000000)
		}
	}

	val moneyTransferRequestValidator = Validation<Requests.MoneyTransferRequest> {
		Requests.MoneyTransferRequest::amount required {
			minimum(1)
			maximum(1000000)
		}

		Requests.MoneyTransferRequest::accountId required {
			minimum(1)
		}

		Requests.MoneyTransferRequest::otherAccountId required {
			minimum(1)
		}
	}
}
