package samz.moneytransfer.api.controllers

import io.konform.validation.Invalid
import io.konform.validation.Valid
import org.http4k.core.*
import org.http4k.format.Jackson
import org.http4k.format.Jackson.json
import org.http4k.lens.RequestContextKey
import samz.moneytransfer.api.controllers.JsonFormatters.asJson
import samz.moneytransfer.api.controllers.Lenses.ChargeTransferRequestLens
import samz.moneytransfer.api.controllers.Lenses.MoneyTransferRequestLens
import samz.moneytransfer.api.controllers.MoneyTransferResponse.*
import samz.moneytransfer.api.controllers.Validators.chargeTransferRequestValidator
import samz.moneytransfer.api.controllers.Validators.moneyTransferRequestValidator
import samz.moneytransfer.api.services.MoneyTransferService


class MoneyTransferController(moneyTransferService: MoneyTransferService, contexts: RequestContexts) {

	// example of nice separation of concerns using request context
	// a filter with validation validates, then if valid the ctrl fn just executes without extra boilerplate code
	// the filter + validation can be abstracted so it can be used easily across all ctrl endpoints
	private val chargeTransferRequestKey = RequestContextKey.required<Requests.ChargeTransferRequest>(contexts)

	private val chargeValidationFilter = Filter { next ->
		{ request ->
			val validationResult = request.let { ChargeTransferRequestLens(it) }
					.let { chargeTransferRequestValidator(it) }
			val a = when (validationResult) {
				is Invalid -> Response(Status.BAD_REQUEST)
						.with(
								Body.json().toLens() of asJson(
										validationResult,
										listOf(Requests.ChargeTransferRequest::amount, Requests.ChargeTransferRequest::accountId)))
				is Valid -> next(request.with(chargeTransferRequestKey of validationResult.value))
			}
			a
		}
	}

	val charge: HttpHandler = chargeValidationFilter.then { request ->
		when (val chargeResult = moneyTransferService.charge(chargeTransferRequestKey(request))) {
			is MoneyTransferPending -> {
				Response(Status.OK).with(Body.json().toLens() of asJson(chargeResult))
			}
			is AccountForTransferNotFound -> Response(Status.BAD_REQUEST).with(
					Body.json().toLens() of Jackson {
						obj("errors" to obj(
								"general" to array(listOf(string("account with id ${chargeResult.accountId} not found")))
						))
					})
			else -> Response(Status.NOT_IMPLEMENTED)
		}
	}

	val transfer: HttpHandler = { request ->

		when (val validationResult = moneyTransferRequestValidator(MoneyTransferRequestLens(request))) {
			is Valid -> {
				when (val transferResponse = moneyTransferService.transfer(validationResult.value)) {
					is MoneyTransferPending -> {
						Response(Status.OK).with(Body.json().toLens() of asJson(transferResponse))
					}
					is SelfTransferNotAllowed -> Response(Status.BAD_REQUEST).with(
							Body.json().toLens() of Jackson {
								obj("errors" to obj(
										"general" to array(listOf(string("Cannot transfer to same account")))
								))
							})
					is AccountForTransferNotFound -> Response(Status.BAD_REQUEST).with(
							Body.json().toLens() of Jackson {
								obj("errors" to obj(
										"general" to array(listOf(string("account with id ${transferResponse.accountId} not found")))
								))
							})
				}
			}
			is Invalid -> {
				Response(Status.BAD_REQUEST)
						.with(
								Body.json().toLens() of asJson(
										validationResult,
										listOf(Requests.MoneyTransferRequest::amount,
												Requests.MoneyTransferRequest::accountId,
												Requests.MoneyTransferRequest::otherAccountId)))
			}
		}
	}

	val getTransferById: HttpHandler = { request ->
		moneyTransferService.getTransferById(Lenses.pathIdIntLens(request))
				?.let {
					Response(Status.OK)
							.with(Body.json().toLens() of asJson(it))
				}
				?: Response(Status.NOT_FOUND)
	}
}
