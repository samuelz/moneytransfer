package samz.moneytransfer.db

import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import samz.moneytransfer.db.orm.Mappers.transferFromRow
import samz.moneytransfer.db.orm.TransfersTable
import samz.moneytransfer.models.Transfer
import samz.moneytransfer.models.TransferState
import samz.moneytransfer.models.TransferType

class TransfersRepo {

	fun createPendingTransfer(amount: Int,
														accountId: Int,
														otherAccountId: Int,
														transferType: TransferType): Int {
		val now = DateTime(DateTimeZone.UTC)
		return transaction {
			TransfersTable.insert {
				it[createdAt] = now
				it[updatedAt] = now
				it[TransfersTable.amount] = amount
				it[TransfersTable.accountId] = accountId
				it[TransfersTable.otherAccountId] = otherAccountId
				it[balanceAfter] = 0 // will be corrected later, probably better as nullable column
				it[lastTransferId] = 0 // will be corrected later, probably better as nullable column
				it[state] = TransferState.PENDING
				it[`type`] = transferType
			} get TransfersTable.id
		}.value
	}

	fun getTransferById(id: Int): Transfer? {
		val row = transaction {
			TransfersTable.select { TransfersTable.id eq id }.firstOrNull()
		}
		return row?.let { transferFromRow(it) }
	}

	fun createInitAccountTransfer(accountId: Int, startingBalance: Int = 0): Int {
		val now = DateTime(DateTimeZone.UTC)
		return transaction {
			TransfersTable.insert {
				it[createdAt] = now
				it[updatedAt] = now
				it[finishedAt] = now
				it[state] = TransferState.EXECUTED
				it[`type`] = TransferType.INIT
				it[amount] = startingBalance
				it[TransfersTable.accountId] = accountId
				it[lastTransferId] = 0 // no transfer before! beginning of log here :)
				it[balanceAfter] = startingBalance
				it[otherAccountId] = accountId
			} get TransfersTable.id
		}.value
	}

	fun getLastExecutedTransferForAccount(accountId: Int): Transfer? {
		val row = transaction {
			TransfersTable.select {
				(TransfersTable.accountId eq accountId) and
						(TransfersTable.state eq TransferState.EXECUTED)
			}.orderBy(TransfersTable.finishedAt to SortOrder.DESC,
					TransfersTable.id to SortOrder.DESC).firstOrNull()
		}
		return row?.let { transferFromRow(it) }
	}


}
