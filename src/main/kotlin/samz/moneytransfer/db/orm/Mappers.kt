package samz.moneytransfer.db.orm

import org.jetbrains.exposed.sql.ResultRow
import samz.moneytransfer.models.Account
import samz.moneytransfer.models.Transfer

object Mappers {
	fun accountFromRow(row: ResultRow): Account {
		return Account(
				id = row[AccountsTable.id].value,
				createdAt = row[AccountsTable.createdAt],
				updatedAt = row[AccountsTable.updatedAt],
				nickname = row[AccountsTable.nickname])
	}

	fun transferFromRow(row: ResultRow): Transfer {
		return Transfer(
				id = row[TransfersTable.id].value,
				createdAt = row[TransfersTable.createdAt],
				updatedAt = row[TransfersTable.updatedAt],
				finishedAt = row[TransfersTable.finishedAt],
				amount = row[TransfersTable.amount],
				balanceAfter = row[TransfersTable.balanceAfter],
				accountId = row[TransfersTable.accountId],
				otherAccountId = row[TransfersTable.otherAccountId],
				`type` = row[TransfersTable.`type`],
				state = row[TransfersTable.state],
				lastTransferId = row[TransfersTable.lastTransferId]
		)
	}
}
