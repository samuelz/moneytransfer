package samz.moneytransfer.db.orm

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

object AccountsTable : IntIdTable("accounts") {
	val createdAt = datetime("created_at").default(DateTime(DateTimeZone.UTC))
	val updatedAt = datetime("updated_at").default(DateTime(DateTimeZone.UTC))
	val nickname = varchar("nickname", length = 50)
	val lastTransferId = (integer("last_transfer_id").references(TransfersTable.id)).nullable()
}


class AccountDTO(id: EntityID<Int>) : IntEntity(id) {
	companion object : IntEntityClass<AccountDTO>(AccountsTable)

	var createdAt by AccountsTable.createdAt
	var updatedAt by AccountsTable.updatedAt
	var nickname by AccountsTable.nickname
	var lastTransferId by AccountsTable.lastTransferId
}
