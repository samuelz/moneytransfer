package samz.moneytransfer.db.orm

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import samz.moneytransfer.models.TransferState
import samz.moneytransfer.models.TransferType

object TransfersTable : IntIdTable("transfers") {
	val createdAt = datetime("created_at").default(DateTime(DateTimeZone.UTC))
	val updatedAt = datetime("updated_at").default(DateTime(DateTimeZone.UTC))
	val finishedAt = datetime("finished_at").nullable()
	val amount = integer("amount")
	val balanceAfter = integer("balance_after")
	val accountId = integer("account_id")
	val otherAccountId = integer("other_account_id")
	val `type` = enumerationByName("transfer_type", 127, TransferType::class)
	val state = enumerationByName("state", 127, TransferState::class)
	val lastTransferId = integer("last_transfer_id")
}

class TransferDto(id: EntityID<Int>) : IntEntity(id) {
	companion object : IntEntityClass<TransferDto>(TransfersTable)

	var createdAt by TransfersTable.createdAt
	val updatedAt by TransfersTable.updatedAt
	val finishedAt by TransfersTable.finishedAt
	val amount by TransfersTable.amount
	val balanceAfter by TransfersTable.balanceAfter
	val accountId by TransfersTable.accountId
	val otherAccountId by TransfersTable.otherAccountId
	val `type` by TransfersTable.`type`
	val state by TransfersTable.state
	val lastTransferId by TransfersTable.lastTransferId
}
