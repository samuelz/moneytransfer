package samz.moneytransfer.db

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import samz.moneytransfer.db.orm.AccountsTable
import samz.moneytransfer.db.orm.Mappers.accountFromRow
import samz.moneytransfer.models.Account

class AccountsRepo {
	fun createAccount(nick: String): Int {
		val now = DateTime(DateTimeZone.UTC)
		return transaction {
			AccountsTable.insert {
				it[nickname] = nick
				it[createdAt] = now
				it[updatedAt] = now
			} get AccountsTable.id
		}.value
	}

	fun updateLastTransferId(accountId: Int, transferId:Int): Int {
		val now = DateTime(DateTimeZone.UTC)
		return transaction {
			AccountsTable.update ({ AccountsTable.id eq accountId }) {
				it[updatedAt] = now
				it[lastTransferId] = transferId
			}
		}
	}

	fun getAccountById(id: Int): Account? {
		val row = transaction {
			AccountsTable.select { AccountsTable.id eq id }.firstOrNull()
		}
		return row?.let { accountFromRow(it) }
	}
}
