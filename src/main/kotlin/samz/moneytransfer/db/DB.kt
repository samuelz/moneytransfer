package samz.moneytransfer.db

import com.opentable.db.postgres.embedded.EmbeddedPostgres
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import samz.moneytransfer.db.orm.AccountsTable
import samz.moneytransfer.db.orm.TransfersTable
import samz.moneytransfer.models.TransferState
import samz.moneytransfer.models.TransferType

object DB {
	private val datasource: HikariDataSource

	init {
		val postgresDb = EmbeddedPostgres.start()

		val hikariCp = HikariConfig()
		hikariCp.apply {
			jdbcUrl = postgresDb.getJdbcUrl("postgres", "postgres")
			// @see https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing#the-formula
			maximumPoolSize = Runtime.getRuntime().availableProcessors() * 2
		}

		datasource = HikariDataSource(hikariCp)

		Database.connect(datasource)

		transaction {
			// DEFINITELY NOT something you'd want to do on production lol
			// on production you'd have a real db and manage setup & migrations with a REAL tool
			// and not with the utility class of the ORM
			SchemaUtils.drop(AccountsTable, TransfersTable)
			SchemaUtils.createMissingTablesAndColumns(AccountsTable, TransfersTable)

			AccountsTable.insert {
				it[nickname] = "Haupt Konto"
			}

			AccountsTable.insert {
				it[nickname] = "Saving for iphone"
			}

			val now = DateTime(DateTimeZone.UTC)

			TransfersTable.insert {
				it[amount] = 100
				it[accountId] = 1
				it[otherAccountId] = 1
				it[balanceAfter] = 100
				it[`type`] = TransferType.CHARGE
				it[state] = TransferState.EXECUTED
				it[finishedAt] = now
				it[createdAt] = now
				it[updatedAt] = now
				it[lastTransferId] = 0
			}

			TransfersTable.insert {
				it[amount] = 50
				it[accountId] = 2
				it[otherAccountId] = 2
				it[balanceAfter] = 50
				it[`type`] = TransferType.CHARGE
				it[state] = TransferState.EXECUTED
				it[finishedAt] = now
				it[createdAt] = now
				it[updatedAt] = now
				it[lastTransferId] = 0
			}
		}
	}

	fun close() = datasource.close()
}
