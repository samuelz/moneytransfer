package samz.moneytransfer.processor

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.slf4j.LoggerFactory
import samz.moneytransfer.db.AccountsRepo
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.db.orm.AccountsTable
import samz.moneytransfer.db.orm.Mappers
import samz.moneytransfer.db.orm.TransfersTable
import samz.moneytransfer.models.Transfer
import samz.moneytransfer.models.TransferState
import samz.moneytransfer.models.TransferType
import java.lang.RuntimeException
import java.sql.Connection
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random


sealed class ProcessorMessage {
	data class TransferMessage(val transferId: Int, val accountId: Int, val retriesLeft: Int = 5) : ProcessorMessage()
	object End : ProcessorMessage()
}


// processer or "consumer" of transfers, performs the actual money laundering
class TransferProcessor(private val queue: LinkedBlockingDeque<ProcessorMessage>) : Runnable {

	private val logger = LoggerFactory.getLogger("TransferProcessor")
	private val transfersRepo = TransfersRepo()


	var shouldRun = AtomicBoolean(true)

	override fun run() {
		logger.info("started")

		try {
			while (shouldRun.get()) {
				when (val msg = queue.take()) {
					is ProcessorMessage.TransferMessage -> processTransferMessage(msg)
					is ProcessorMessage.End -> {
						logger.info("got END msg, shutting down")
						shouldRun.set(false)
					}
				}
			}
			logger.info("stopped: shouldRun was false")

		} catch (ex: InterruptedException) {
			logger.error("interrupted", ex)
			Thread.currentThread().interrupt()
			throw RuntimeException(ex)
		}
	}

	fun stop() {
		logger.info("stop called: putting END message into front of queue")
		queue.putFirst(ProcessorMessage.End)
	}


	private fun processTransferMessage(msg: ProcessorMessage.TransferMessage) {
		logger.debug("processing message $msg")

		val transferOpt = transfersRepo.getTransferById(msg.transferId)
		if (transferOpt == null) {
			logger.info("transfer not found: ${msg.transferId}, retries left: ${msg.retriesLeft}")
			if (msg.retriesLeft > 0)
				return queue.putLast(msg.copy(retriesLeft = msg.retriesLeft - 1))
			else
				return
		}

		val transfer = transferOpt!!

		if (transfer.state != TransferState.PENDING) {
			logger.info("transfer is not pending: ${msg.transferId}")
			return
		}

		// optional comment in:
		//   if order of transfer execution is important to be the same as transfer creation (per account)
		//   then this commented section will prioritize transfers of an account that have earlier created dates
		//   by returning the msg to the queue
		// problem: if the earlier transfer failed somehow and the msg endlessly goes back in queue
		// possible solution: introduce "retries" in msg, increase with each re-entry
		//                    and after a couple (5?) let transfer go forward anyway
//		val higherPrioPendingTransfer = transaction {
//			TransfersTable.select {
//				(TransfersTable.accountId eq transfer.accountId) and
//						(TransfersTable.state inList listOf(TransferState.PENDING)) and
//						(TransfersTable.createdAt less transfer.createdAt)
//			}.firstOrNull()
//		}
//		if (higherPrioPendingTransfer != null) {
//			logger.info(" ${transfer.id} > ${higherPrioPendingTransfer[TransfersTable.id]} found pending transfer with higher prio, retuning msg to queue.")
//			GlobalScope.launch {
//				delay(200)
//				queue.putLast(msg)
//			}
//			return
//		}

		//	update state to IN_PROGRESS, where state is PENDING
		//	only the first worker that is able to update continues forward.
		//	this is a "race condition" (or fence) that prevents multiple workers from processing the same transfer
		//  generally speaking: this doesn't happen with the stupid java queue, but could happen on other messaging systems
		//  where msg delivery to a single consumer is not 100% promised
		val rowsChanged = transaction {
			TransfersTable.update({
				(TransfersTable.id eq transfer.id) and (TransfersTable.state eq TransferState.PENDING)
			}) {
				it[updatedAt] = DateTime(DateTimeZone.UTC)
				it[state] = TransferState.IN_PROGRESS
			}
		}
		if (rowsChanged == 0) {
			logger.info("transfer already handled by another processor: ${msg.transferId}")
			return
		}

		return executeTransfer(transfer, msg)
	}

	tailrec fun retryTransfer(transfer: Transfer, msg: ProcessorMessage.TransferMessage, retriesLeft: Int) {
		logger.info("retryTransfer $retriesLeft: id: ${transfer.id}, account: ${transfer.accountId}")

		Thread.sleep(100L * (5 - retriesLeft))
		return if (retriesLeft == 0) {
			transaction {
				TransfersTable.update({
					(TransfersTable.id eq transfer.id)
				}) {
					it[updatedAt] = DateTime(DateTimeZone.UTC)
					it[state] = TransferState.PENDING
				}
			}
			queue.putLast(msg)
		} else {
			executeTransfer(transfer, msg, retriesLeft - 1)
		}
	}


	tailrec fun executeTransfer(transfer: Transfer, msg: ProcessorMessage.TransferMessage, retriesLeft: Int = 5) {
		var rowsChanged: Int = 0

		try {
			transaction(Connection.TRANSACTION_REPEATABLE_READ, 100) {
				// start a "select for update" on account id, use db as lock
				val account = AccountsTable.select({ AccountsTable.id eq transfer.accountId }).forUpdate().first().let { Mappers.accountFromRow(it) }

				// load last executed transfer, check balance after
				// NOTE, made problem a little easier by forcing "last transfer" is not null
				// and putting an initial transfer for each account created
				val lastTransfer = transfersRepo.getLastExecutedTransferForAccount(transfer.accountId)!!
				val lastBalance = lastTransfer.balanceAfter

				logger.info("execute transfer in txn with ${transfer.type} id: ${transfer.id}, acct: ${transfer.accountId}, dest: ${transfer.otherAccountId}, lastT: ${lastTransfer.id} ")
				// setting a precondition for later updates
				// will only execute and update balances as long as lastTransfer remains unchanged
				logger.info("setting last transfer of ${transfer.id} to ${lastTransfer.id}")

				TransfersTable.update({
					(TransfersTable.id eq transfer.id)
				}) {
					it[updatedAt] = DateTime(DateTimeZone.UTC)
					it[lastTransferId] = lastTransfer.id
				}


				if (transfer.`type` == TransferType.CHARGE) {
					// if 0 lastTransfer has changed (and balance might have as well): need to retry
					rowsChanged = finishTransferUpdate(
							transfer,
							lastBalance + transfer.amount,
							TransferState.EXECUTED)
				} else if (transfer.`type` == TransferType.TRANSFER_CREDIT) {
					rowsChanged = finishTransferUpdate(
							transfer,
							lastBalance + transfer.amount,
							TransferState.EXECUTED)

				} else if (transfer.`type` == TransferType.TRANSFER_DEBIT) {
					// insufficient funds check
					if (transfer.amount > lastBalance) {
						// real app: should save fail reason as well, or expand TransferState to be
						// some sort of "code" that describes both state + resolution such as
						//   insufficient funds, limit reached, account blocked etc
						rowsChanged = finishTransferUpdate(
								transfer,
								lastBalance,
								TransferState.FAILED)
						logger.info("transfer FAILED due to insufficient funds: id: ${transfer.id}")
					} else {
						rowsChanged = finishTransferUpdate(
								transfer,
								lastBalance - transfer.amount,
								TransferState.EXECUTED)

						transfersRepo.createPendingTransfer(
								transfer.amount,
								transfer.otherAccountId,
								transfer.accountId,
								TransferType.TRANSFER_CREDIT).let {
							queue.putFirst(ProcessorMessage.TransferMessage(it, transfer.otherAccountId))
						}
					}
				} else {
					logger.warn("unknown transfer type: $transfer")
				}

				if (rowsChanged != 0) {
					AccountsTable.update({ AccountsTable.id eq transfer.accountId }) {
						it[updatedAt] = DateTime(DateTimeZone.UTC)
						it[lastTransferId] = transfer.id
					}
				}
			}
		} catch (e: Exception) {
			logger.error("ERRRRRRRRRR for id ${transfer.id}", e)
		}


		if (rowsChanged == 0) {
			return retryTransfer(transfer, msg, retriesLeft)
		} else {
			logger.info("executed ${transfer.type}, transfer id: ${transfer.id}, account id: ${transfer.accountId}.")
		}

	}

	/**
	 * returns number of rows affected
	 */
	internal fun finishTransferUpdate(transfer: Transfer,
																		newBalance: Int,
																		state: TransferState): Int {
		val now = DateTime(DateTimeZone.UTC)

		val lastTransferIdQuery = lastTransferQuery(transfer.accountId)
		return TransfersTable.update({
			(TransfersTable.id eq transfer.id) and
					(TransfersTable.state eq TransferState.IN_PROGRESS) and
					(TransfersTable.lastTransferId inSubQuery (lastTransferIdQuery))
		}) {
			it[updatedAt] = now
			it[finishedAt] = now
			it[TransfersTable.state] = state
			it[balanceAfter] = newBalance
		}

	}

	private fun lastTransferQuery(accountId: Int): Query {
		return TransfersTable.slice(TransfersTable.id).select {
			(TransfersTable.accountId eq accountId) and
					(TransfersTable.state eq TransferState.EXECUTED)
		}.orderBy(TransfersTable.finishedAt to SortOrder.DESC,
				TransfersTable.id to SortOrder.DESC).limit(1)
	}
}
