package samz.moneytransfer.models

import org.joda.time.DateTime

data class Account(
		val id: Int,
		val createdAt: DateTime,
		val updatedAt: DateTime,
		val nickname: String
)



