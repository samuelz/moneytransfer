package samz.moneytransfer.models

import org.joda.time.DateTime

enum class TransferState {
	PENDING, IN_PROGRESS, EXECUTED, FAILED
}

enum class TransferType {
	INIT, // first transfer created at account creation
	CHARGE, // user charges his account
	// transfer between 2 accounts, debitor && creditor side
	//	can later be replaced or expanded with SEPA, P2P and more
	TRANSFER_DEBIT,
	TRANSFER_CREDIT
//  more possible values:
//	CC_CREDIT, // user is credited money via card
//	CC_DEBIT   // user is debited money via card
}

data class Transfer(
		val id: Int,
		val createdAt: DateTime,
		val updatedAt: DateTime,
		val finishedAt: DateTime?,
		val amount: Int,
		val balanceAfter: Int,
		val accountId: Int,
		val otherAccountId: Int,
		val `type`: TransferType,
		val state: TransferState,
		val lastTransferId: Int
)



