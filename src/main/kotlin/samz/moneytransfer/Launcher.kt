package samz.moneytransfer

import org.slf4j.LoggerFactory
import samz.moneytransfer.api.ApiBackend
import samz.moneytransfer.db.DB
import samz.moneytransfer.processor.ProcessorMessage
import samz.moneytransfer.processor.TransferProcessor
import sun.misc.Signal
import java.util.concurrent.LinkedBlockingDeque


object Launcher {

	private val logger = LoggerFactory.getLogger("orchestrator")
	// the shared queue for processing transfers, limited to 10000 for sake of this example task
	private val sharedBlockingQueue = LinkedBlockingDeque<ProcessorMessage>(10000)
	private val db: DB = DB
	private val apiBackend: ApiBackend = ApiBackend(9000, sharedBlockingQueue)

	private const val NUMBER_OF_TRANSFER_PROCESSORS = 4
	private val processors = (1..NUMBER_OF_TRANSFER_PROCESSORS).map { TransferProcessor(sharedBlockingQueue) }
	private val processorThreads = processors.map { Thread(it) }


	fun start() {
		logger.debug("starting webserver")
		apiBackend.start()
		processorThreads.forEach { it.start() }
	}

	fun stop() {
		logger.info("stopping: graceful shutdown")
		logger.debug("shutting down webserver")
		apiBackend.stop()
		logger.debug("shutting down transfer processor")
		processors.forEach { it.stop() }
		logger.debug("shutting down datasource")
		db.close()
	}

}

fun main(args: Array<String>) {
	Launcher.start()

	Signal.handle(Signal("INT")) {
		Launcher.stop()
	}
}
