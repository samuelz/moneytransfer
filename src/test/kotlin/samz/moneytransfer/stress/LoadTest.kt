package samz.moneytransfer.stress

import io.kotlintest.milliseconds
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.http4k.client.OkHttp
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.format.Jackson
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.db.orm.TransferDto
import samz.moneytransfer.db.orm.TransfersTable
import samz.moneytransfer.models.TransferState
import samz.moneytransfer.models.TransferType

//

class LoadTest : DescribeSpec() {

	val client = OkHttp()
	val transfersRepo = TransfersRepo()

	val initialAmountPerAccount = 5000
	val numOfAccounts = 10
	val numOfTransfersPerAccount = 200
	val maxSleep = 5000 + (numOfTransfersPerAccount * numOfAccounts * 100L)

// some benchmarks from testing on my computer
// ## 10 acounts, 200 transfers each
// ~90 seconds WITH order significance
// ~20 seconds WITHOUT order significan
// ## 20 acounts, 100 transfers each
// ~45 seconds WITH order significance
// ~20 seconds WITHOUT order significance
// ## 50 accounts, 40 transfers each
// ~40 seconds WITH order significance
// ~20 seconds WITHOUT order significance
// ## 200 accounts, 10 transfers each (looks like ~30 seconds tes setup time)
// ~60 seconds WITH order significance
// ~60 seconds WITHOUT order significance

	init {
		describe("load/stress test the whole system with many transfers") {
			it("will not lose money").config(timeout = maxSleep.milliseconds) {
				fun createAccount(nick: String): Int {
					val resp = client(Request(Method.POST, "http://localhost:9000/account").body(
							Jackson.obj("nickname" to Jackson.string(nick)).toString()
					))
					val json = Jackson.parse(resp.bodyString())
					return json["id"].intValue()
				}

				fun chargeAccount(accountId: Int, amount: Int): Int {
					val response = client(Request(Method.POST, "http://localhost:9000/money/charge").body(
							Jackson {
								obj(
										"amount" to number(amount),
										"accountId" to number(accountId)
								)
							}.toString()
					))

					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					return json["id"].intValue()
				}

				fun createTransfer(accountId: Int, otherAccountId: Int, amount: Int): Int {
					val response = client(Request(Method.POST, "http://localhost:9000/money/transfer").body(
							Jackson {
								obj(
										"amount" to number(amount),
										"accountId" to number(accountId),
										"otherAccountId" to number(otherAccountId)
								)
							}.toString()
					))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					return json["id"].intValue()
				}


				val accountIds = (1..numOfAccounts).map {
					createAccount("nick_$it").also { accountId ->
						chargeAccount(accountId, initialAmountPerAccount)
					}
				}


				val initialBalanceInSystem = accountIds.size * initialAmountPerAccount

				// some safety time for processor to execute the charges
				Thread.sleep(accountIds.size * 200L)


				val accountIdToOtherAccountsPairs = accountIds.zip(accountIds.map { accountId -> accountIds.filterNot { it == accountId } })

				// repeatedly create transfers
				GlobalScope.launch {
					repeat(numOfTransfersPerAccount) {
						accountIdToOtherAccountsPairs.forEach { pair ->
							val otherAcct = pair.second.random()
							createTransfer(pair.first, otherAcct, 1)
						}
					}
				}

				// some safety time for processor to execute the transfers
				// this is terrible ofcourse for a real system
				// but didn't want to spend too much time here
				// this is better replaced by some repeat-delayed-wait for end condition (db check)
				// or by spying something that is related to end condition or anything really :)

				var lastTransferFinished = false
				var slept = 0

				while (!lastTransferFinished) {
					Thread.sleep(1000)
					slept += 1000
					if (slept > maxSleep) {
						break
					}

					transaction {
						val countPending = TransferDto.find { TransfersTable.state inList listOf(TransferState.PENDING, TransferState.IN_PROGRESS) }.count()
						if (countPending == 0) lastTransferFinished = true
					}


				}
				// check that `numOfTransfers` * debit/credit sides have been executed for each account
				fun countTransfers(accountId: Int, transferType: TransferType): Int {
					return transaction {
						TransfersTable.slice(TransfersTable.id).select {
							(TransfersTable.accountId eq accountId) and
									(TransfersTable.state eq TransferState.EXECUTED) and
									(TransfersTable.`type` eq transferType)
						}.count()
					}
				}

				accountIds.forEach { accountId ->
					countTransfers(accountId, TransferType.TRANSFER_DEBIT) shouldBe numOfTransfersPerAccount
				}

				val totalBalanceAfter = accountIds.map {
					transfersRepo.getLastExecutedTransferForAccount(it)?.balanceAfter ?: 0
				}.sum()

				totalBalanceAfter shouldBe (initialBalanceInSystem)

			}
		}
	}
}
