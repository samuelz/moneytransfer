package samz.moneytransfer.api

import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import org.http4k.client.OkHttp
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status


class WebServerSanityTest : DescribeSpec() {

	val client = OkHttp()

	init {
		describe("web server") {
			it("responds to /ping") {
				val response = client(Request(Method.GET, "http://localhost:9000/ping"))
				response.status shouldBe Status.OK
				response.bodyString() shouldBe "pong"
			}
		}
	}
}
