package samz.moneytransfer.api

import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import org.http4k.client.OkHttp
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.format.Jackson
import org.http4k.format.Jackson.number
import org.http4k.format.Jackson.string


class AccountTest : DescribeSpec() {

	val client = OkHttp()

	init {
		describe("Accounts") {

			describe("create") {
				it("with valid params") {
					val response = client(Request(Method.POST, "http://localhost:9000/account").body(
							Jackson.obj("nickname" to string("nicky minaj")).toString()
					))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					json["id"] shouldBe number(3)
					json["createdAt"].shouldNotBeNull()
					json["updatedAt"].shouldNotBeNull()
					json["nickname"] shouldBe string("nicky minaj")
				}

				it("with invalid params: nickname too long") {
					val response = client(Request(Method.POST, "http://localhost:9000/account").body(
							Jackson.obj("nickname" to string("nicky minajnicky minajnicky minajnicky minajnicky minajnicky minajnicky minajnicky minajnicky minajnicky minaj")).toString()
					))
					response.status shouldBe Status.BAD_REQUEST
					val json = Jackson.parse(response.bodyString())
					json["errors"].shouldNotBeNull()
					json["errors"]["nickname"].shouldNotBeNull()
					json["errors"]["nickname"][0] shouldBe string("must have at most 50 characters")
				}

				it("with empty body params") {
					val response = client(Request(Method.POST, "http://localhost:9000/account"))
					response.status shouldBe Status.BAD_REQUEST
				}
			}

			describe("get by id") {
				it("with valid account id for an existing account") {
					val response = client(Request(Method.GET, "http://localhost:9000/account/1"))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					json["id"] shouldBe number(1)
					json["createdAt"].shouldNotBeNull()
					json["updatedAt"].shouldNotBeNull()
					json["nickname"] shouldBe string("Haupt Konto")
				}

				it("with valid account id for non existing account") {
					val response = client(Request(Method.GET, "http://localhost:9000/account/999"))
					response.status shouldBe Status.NOT_FOUND
				}

				it("with invalid account id") {
					val response = client(Request(Method.GET, "http://localhost:9000/account/one"))
					response.status shouldBe Status.BAD_REQUEST
				}
			}
		}
	}
}
