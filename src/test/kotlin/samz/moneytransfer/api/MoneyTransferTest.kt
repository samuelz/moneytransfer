package samz.moneytransfer.api

import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import io.kotlintest.specs.DescribeSpec
import org.http4k.client.OkHttp
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.format.Jackson
import samz.moneytransfer.db.TransfersRepo
import samz.moneytransfer.models.Transfer
import samz.moneytransfer.models.TransferState


class MoneyTransferTest : DescribeSpec() {

	val client = OkHttp()
	val transfersRepo = TransfersRepo()

	init {
		describe("money transfers") {

			describe("charge") {
				it("creates a pending transfer") {
					val lastBalanceAccount = transfersRepo.getLastExecutedTransferForAccount(1)?.balanceAfter ?: 0

					val response = client(Request(Method.POST, "http://localhost:9000/money/charge").body(
							Jackson {
								obj(
										"amount" to number(2),
										"accountId" to number(1)
								)
							}.toString()
					))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					json["id"].shouldNotBeNull()

					val finisedTransfer = waitForTransfer(json["id"].intValue(), TransferState.EXECUTED)
					finisedTransfer.state shouldBe TransferState.EXECUTED
					finisedTransfer.amount shouldBe 2
					finisedTransfer.balanceAfter shouldBe (lastBalanceAccount + 2)

				}


				it("return errors on invalid params") {
					val response = client(Request(Method.POST, "http://localhost:9000/money/charge").body(
							Jackson {
								obj(
										"amount" to number(-10),
										"accountId" to number(1)
								)
							}.toString()
					))
					response.status shouldBe Status.BAD_REQUEST
					val json = Jackson.parse(response.bodyString())
					json["errors"].shouldNotBeNull()
					json["errors"]["amount"].shouldNotBeNull()
					json["errors"]["amount"][0] shouldBe Jackson.string("must be at least '1'")
				}

				it("return error on non existing account") {
					val response = client(Request(Method.POST, "http://localhost:9000/money/charge").body(
							Jackson {
								obj(
										"amount" to number(2),
										"accountId" to number(15)
								)
							}.toString()
					))
					response.status shouldBe Status.BAD_REQUEST
					val json = Jackson.parse(response.bodyString())
					json["errors"].shouldNotBeNull()
					json["errors"]["general"].shouldNotBeNull()
					json["errors"]["general"][0] shouldBe Jackson.string("account with id 15 not found")
				}
			}


			describe("inter-account transfer") {
				it("creates a pending transfer that gets executed") {
					val lastBalanceAccount = transfersRepo.getLastExecutedTransferForAccount(1)?.balanceAfter ?: 0
					val lastBalanceOtherAccount = transfersRepo.getLastExecutedTransferForAccount(2)?.balanceAfter ?: 0

					val response = client(Request(Method.POST, "http://localhost:9000/money/transfer").body(
							Jackson {
								obj(
										"amount" to number(2),
										"accountId" to number(1),
										"otherAccountId" to number(2)
								)
							}.toString()
					))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					json["id"].shouldNotBeNull()

					val finisedTransfer = waitForTransfer(json["id"].intValue(), TransferState.EXECUTED)
					finisedTransfer.state shouldBe TransferState.EXECUTED
					finisedTransfer.amount shouldBe 2
					finisedTransfer.balanceAfter shouldBe (lastBalanceAccount - 2)

					transfersRepo.getLastExecutedTransferForAccount(2)?.run {
						balanceAfter shouldBe lastBalanceOtherAccount + 2
					}

				}

				it("creates a pending transfer that fails when insufficient funds") {
					val response = client(Request(Method.POST, "http://localhost:9000/money/transfer").body(
							Jackson {
								obj(
										"amount" to number(20000),
										"accountId" to number(1),
										"otherAccountId" to number(2)
								)
							}.toString()
					))
					response.status shouldBe Status.OK
					val json = Jackson.parse(response.bodyString())
					json["id"].shouldNotBeNull()

					val finisedTransfer = waitForTransfer(json["id"].intValue(), TransferState.FAILED)
					finisedTransfer.state shouldBe TransferState.FAILED
					finisedTransfer.amount shouldBe 20000
				}


				it("return errors on invalid params") {
					val response = client(Request(Method.POST, "http://localhost:9000/money/transfer").body(
							Jackson {
								obj(
										"amount" to number(-10),
										"accountId" to number(0),
										"otherAccountId" to number(-1)
								)
							}.toString()
					))
					response.status shouldBe Status.BAD_REQUEST
					val json = Jackson.parse(response.bodyString())
					json["errors"].shouldNotBeNull()
					json["errors"]["amount"].shouldNotBeNull()
					json["errors"]["amount"][0] shouldBe Jackson.string("must be at least '1'")
				}

				it("return error on when account and other account are the same") {
					val response = client(Request(Method.POST, "http://localhost:9000/money/transfer").body(
							Jackson {
								obj(
										"amount" to number(2),
										"accountId" to number(1),
										"otherAccountId" to number(1)
								)
							}.toString()
					))
					response.status shouldBe Status.BAD_REQUEST
					val json = Jackson.parse(response.bodyString())
					json["errors"].shouldNotBeNull()
					json["errors"]["general"].shouldNotBeNull()
					json["errors"]["general"][0] shouldBe Jackson.string("Cannot transfer to same account")
				}
			}
		}
	}

	fun waitForTransfer(transferId: Int,
											expectedState: TransferState,
											retries: Int = 10,
											delayBetweenRetries: Long = 200): Transfer {
		val transfer = transfersRepo.getTransferById(transferId)!!

		return when {
			transfer.state == expectedState -> transfer
			retries > 0 -> {
				Thread.sleep(delayBetweenRetries)
				waitForTransfer(transferId, expectedState, retries - 1, delayBetweenRetries)
			}
			else -> transfer
		}

	}

}
