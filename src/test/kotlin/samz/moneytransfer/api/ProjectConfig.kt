package io.kotlintest.provided

import io.kotlintest.AbstractProjectConfig
import samz.moneytransfer.Launcher


object ProjectConfig : AbstractProjectConfig() {

	private var started: Long = 0

	override fun beforeAll() {
		started = System.currentTimeMillis()
		Launcher.start()
	}

	override fun afterAll() {
		Launcher.stop()

		val time = System.currentTimeMillis() - started
		println("overall time [ms]: " + time)
	}
}
